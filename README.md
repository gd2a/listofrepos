# ListOfRepos
I'm going to put the list of repos that the group has access to

* John

1. [GD2OOPFirstWeek](https://bitbucket.org/johnloane/gd2oopfirstweek)

2. [GD2TestCircleCI](https://bitbucket.org/johnloane/gd2testcircleci)

3. [GD2BankAccountInheritance](https://bitbucket.org/johnloane/gd2bankaccountinheritance)

4. [GD2VehicleInheritance](https://bitbucket.org/johnloane/gd2vehicleinheritance)

5. [GD2aCA1Solutions](https://bitbucket.org/johnloane/gd2ca1solution)

6. [GD2Exceptions](https://bitbucket.org/johnloane/gd2exceptions)

7. [GD2ExceptionsTwo](https://bitbucket.org/johnloane/gd2exceptionstwo)

8. [GD2FileIOPartOne](https://bitbucket.org/johnloane/gd2fileiopartone)

9. [GD2CA3Help](https://bitbucket.org/johnloane/gd2ca3help)

10. [GD2Regex](https://bitbucket.org/johnloane/gd2regex)

11. [GD2SnailBait](https://bitbucket.org/johnloane/gd2snailbait)